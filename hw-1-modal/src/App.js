import React, { Component, Fragment } from "react";
import "./assets/styles/App.scss";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstModal: false,
      secondModal: false,
    };
  }

  changeFirstModal = () => {
    this.setState(() => ({
      firstModal: !this.state.firstModal,
    }));
  };
  changeSecondModal = () => {
    this.setState(() => ({
      secondModal: !this.state.secondModal,
    }));
  };
  render() {
    return (
      <Fragment>
        {this.state.firstModal && (
          <Modal
            header={"Do you want to delete this file?"}
            text={
              "Once you delete this file, it won’t be possible to undo this action.Are you sure you want to delete it?"
            }
            closeButton
            closeModal={this.changeFirstModal}
            actions={
              <>
                <Button openModal={this.changeFirstModal}  text={"ok"} color={"#b3382c"} />
                <Button openModal={this.changeFirstModal} text={"cancel"} color={"#b3382c"} />
              </>
            }
          />
        )}
        {this.state.secondModal && (
          <Modal
            header={"Do you want learn React?"}
            text={"Are you Ready?"}
            closeButton
            closeModal={this.changeSecondModal}
            actions={
              <>
                <Button openModal={this.changeSecondModal}  text={"ok"} color={"#5c108e"} />
                <Button openModal={this.changeSecondModal} text={"cancel"} color={"#5c108e"} />
              </>
            }
          />
        )}
        <div className="content">
          <Button
            openModal={this.changeFirstModal}
            color={"blue"}
            text={"Open first modal"}
          />
          <Button
            openModal={this.changeSecondModal}
            color={"green"}
            text={"Open second modal"}
          />
        </div>
      </Fragment>
    );
  }
}

export default App;
