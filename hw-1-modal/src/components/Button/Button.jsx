import React, {Component} from "react";


class Button extends Component {

  render() {
    const { text, color, openModal} = this.props;
    return (
      <button onClick={openModal} style={{ backgroundColor: color }}>
        {text}
      </button>
    );
  }
}


export default Button;
