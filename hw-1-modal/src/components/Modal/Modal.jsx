import React, {Component} from "react";
import "../../assets/styles/Modal.scss";

class Modal extends Component {

  render() {
    let { header, text, closeButton, actions, closeModal } = this.props;
    return (
      <div onClick={closeModal} className={"container_modal"}>
        <div onClick={(e)=> e.stopPropagation()} className="modal">
          <div className="modal_header">
            {header}
            <span onClick={closeModal} className="modal_close">
              {!closeButton || "X"}
            </span>
          </div>
          <div className="modal_text">
            <p>{text}</p>
          </div>
          <div className="modal_buttons">{actions}</div>
        </div>
      </div>
    );
  }
}


export default Modal;
