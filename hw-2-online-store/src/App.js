import React, { Component } from 'react'
import './App.css'
import Modal from './component/Modal/Modal'
import Product from './component/Main/ProductsList/Product/Product'
import ProductsList from './component/Main/ProductsList/ProductsList'
import Button from './component/Button/Button'
class App extends Component {
  state = {
    products: [],
    isOpen: false,
    cardId : null,
  }


  changeModal = (article) => {
    
    if(article) {
      this.setState({
        ...this.state,
        isOpen: !this.state.isOpen,
        cardId : article,
      })
    }else {
      this.setState({
        ...this.state,
        isOpen: !this.state.isOpen,
        cardId : null,
      })
    }
   
  }
  
  setShoppingBasket =(card) =>{
    this.setState({
      ...this.state,
      isOpen: !this.state.isOpen,
      
    })
    const addCardInfo = JSON.parse(localStorage.getItem('cardsData'));
    const product = this.state.products.find((product) => product.article === card)
    addCardInfo.push(product)
    localStorage.setItem('cardsData', JSON.stringify(addCardInfo))
  }
  componentDidMount() {
    fetch(`products.json`)
      .then((response) => {
        return response.json()
      })
      .then((products) => {
        this.setState({
          products: products.products,
        })
      }
     
      )
      if(localStorage.length == 0) {
        localStorage.setItem("cardsData" ,JSON.stringify([]))
      }   
  }
  render() {
    let cards = this.state.products
    return (
      <>
        {this.state.isOpen && (
          <Modal
            header={'want to buy this product ?'}
            text={'add this product in your shopping basket'}
            closeButton={true}
            closeModal={this.changeModal}
            actions={
              <>
                <Button
                  openModal={() =>this.setShoppingBasket(this.state.cardId)}
                  text={'ok'}
                  color={'grey'}
                />
                <Button
                  openModal={this.changeModal}
                  text={'cancel'}
                  color={'grey'}
                />
              </>
            }

          />
        )}
        <section className="main_content">
          <ProductsList
            content={cards.map((item, key) => {  
              return (
                <Product
                  key={key}
                  title={item.title}
                  img={item.url}
                  article={item.article}
                  openModal={() =>this.changeModal(item.article)}
                  color={item.color}
                  price={item.price}
                />
              )
            })}
          />
        </section>
      </>
    )
  }
}

export default App
