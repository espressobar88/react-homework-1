'jsx-a11y/anchor-is-valid';
import React, { Component } from 'react'
import Button from '../../../Button/Button'
import '../../../../assets/styles/Product.scss'
import PropTypes from "prop-types"

class Product extends Component {

  state = {
    favorit : false,
    color : "",
    key : this.props.article,
  }

  changeFavoritStatus = (e) => { 
    const {favorit} = this.state
this.setState({
  ...this.state,
  favorit : !this.state.favorit,
  color : !favorit ?  "yellow" : this.props.color
}, ()=>{
  localStorage.setItem(`${this.props.article}` ,JSON.stringify({
    'color': this.state.color,
    "favorit" : this.state.favorit
  }))
})


  }

componentDidMount() {  
for (const key in localStorage) {
  if(key == this.state.key) {
    let test = JSON.parse(localStorage.getItem(`${this.props.article}`))
    this.setState({
      color : test.color ,
      favorit : test.favorit
    })
  }
  
}

} 
  render() {
    const { title, img, article, openModal, price } = this.props
    return (
      <li className="list_item">
        <img src={img} className="list_item_img" alt="" />
        <h3>{title}</h3>

        <div className="list_info">
          <Button openModal={openModal}   text="add to card" color="#ffdb33" />
          <span>Артикул : {article}</span>

          <a onClick={() =>this.changeFavoritStatus(article)} className="info_icon_star">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill={this.state.color}
            >
              <path d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z" />
            </svg>
          </a>
          <span className="info_price">Цена : {price}</span>
        </div>
      </li>
    )
  }
}

Product.propTypes = {
  title: PropTypes.string,
  article : PropTypes.number,
  openModal : () => {},
  price : PropTypes.string,
  img : PropTypes.string
}

export default Product
