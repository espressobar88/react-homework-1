import React, { Component } from "react";
import PropTypes from "prop-types"
import "../../../assets/styles/ProductsList.scss";

export class ProductsList extends Component {
  render() {
    const { content } = this.props;
    return <ul className="product_list">{content}</ul>;
  }
}


ProductsList.propTypes = {
 content : () => {}
}
export default ProductsList;
