import React from "react";
import "../../assets/styles/Modal.scss";
import PropTypes from "prop-types"

const Modal = (props) => {
  let { header, text, closeButton, actions, closeModal } = props;
  if (closeButton) {
    closeButton = "X";
  }
  return (
    <div onClick={closeModal} className={"container_modal"}>
      <div onClick={(e) => e.stopPropagation()} className="modal">
        <div className="modal_header">
          {header}
          <span onClick={closeModal} className="modal_close">
            {closeButton}
          </span> 
        </div>
        <div className="modal_text">
          <p>{text}</p>
        </div>
        <div className="modal_buttons">{actions}</div>
      </div>
    </div>
  );
};

Modal.propTypes = {
  header: PropTypes.string,
  text : PropTypes.string,
  closeButton : () => {},
  closeModal : () => {},
  actions : () => {}
}



export default Modal;
