import React, {useState,useEffect} from 'react'
import './App.css'
import Product from './component/Main/ProductsList/Product/Product'
import ProductsList from './component/Main/ProductsList/ProductsList'
import Button from './component/Button/Button'
import Header from './component/Header/Header'
import { Route } from 'react-router-dom'
import FavoritPage from './component/Main/pages/Favorit/FavoritPage'
import ShopBasketPage from './component/Main/pages/Basket/ShopBasketPage'

 const App = () => {
  
   let [product , setProduct] = useState([])
  let [addCardInShop , setShopCard] = useState(false)
   useEffect(() => {
    fetch(`products.json`)
      .then((response) => {
        return response.json()
      })
      .then((products) => {
        setProduct(products.products)  
                  localStorage.setItem("cardsData" ,JSON.stringify(products.products))
                
                  if(localStorage.getItem("cardsInShop") == null) {
                    localStorage.setItem("cardsInShop" ,JSON.stringify([]))
                  } 
                if(localStorage.getItem("cardsFavorit") == null) {
                  localStorage.setItem("cardsFavorit" ,JSON.stringify([]))
                } 
              
      }
  )},[]);

const addInShopBasket =(article) => {
  setShopCard(!addCardInShop);
    const getCardsInShop = JSON.parse(localStorage.getItem("cardsInShop"));
    if(!addCardInShop) { 
      getCardsInShop.push(article)
      localStorage.setItem("cardsInShop" ,JSON.stringify(getCardsInShop))
      
    } else {
   const setCardsInShop = getCardsInShop.filter((product) => product !== article )
   localStorage.setItem("cardsInShop" ,JSON.stringify(setCardsInShop))
    }
}
 
  return (
    <>
    <Header/>
        <section className="main_content">
         <Route path="/home" render={() => <ProductsList
            content={product.map((item, key) => {  
              return (
                <Product
                  key={key}
                  title={item.title}
                  img={item.url}
                  article={item.article}
                  price={item.price}
                  action={<Button  openModal={() => addInShopBasket(item.article)} text="add to card" color="#ffdb33" />}
                   />
              )
              
            })}
           
          />}/>
       <Route path="/favorit" render={() => <FavoritPage />}/>
        <Route path="/shopBasket" render={() => <ShopBasketPage/>}/>
         
        </section>
        
        </>
    
   
  )
}

// class App extends Component {
//   state = {
//     products: [],
//     isOpen: false,
//     cardId : null,
//   }


//   changeModal = (article) => {
    
//     if(article) {
//       this.setState({
//         ...this.state,
//         isOpen: !this.state.isOpen,
//         cardId : article,
//       })
//     }else {
//       this.setState({
//         ...this.state,
//         isOpen: !this.state.isOpen,
//         cardId : null,
//       })
//     }
   
//   }
  
//   setShoppingBasket =(card) =>{
//     this.setState({
//       ...this.state,
//       isOpen: !this.state.isOpen,
      
//     })
//     const addCardInfo = JSON.parse(localStorage.getItem('cardsData'));
//     const product = this.state.products.find((product) => product.article === card)
//     addCardInfo.push(product)
//     localStorage.setItem('cardsData', JSON.stringify(addCardInfo))
//   }
//   componentDidMount() {
//     fetch(`products.json`)
//       .then((response) => {
//         return response.json()
//       })
//       .then((products) => {
//         this.setState({
//           products: products.products,
//         })
//       }
     
//       )
//       if(localStorage.length == 0) {
//         localStorage.setItem("cardsData" ,JSON.stringify([]))
//       }   
//   }
//   render() {
//     let cards = this.state.products
//     return (
//       <>
//         {this.state.isOpen && (
//           <Modal
//             header={'want to buy this product ?'}
//             text={'add this product in your shopping basket'}
//             closeButton={true}
//             closeModal={this.changeModal}
//             actions={
//               <>
//                 <Button
//                   openModal={() =>this.setShoppingBasket(this.state.cardId)}
//                   text={'ok'}
//                   color={'grey'}
//                 />
//                 <Button
//                   openModal={this.changeModal}
//                   text={'cancel'}
//                   color={'grey'}
//                 />
//               </>
//             }

//           />
//         )}
//         <section className="main_content">
//           <ProductsList
//             content={cards.map((item, key) => {  
//               return (
//                 <Product
//                   key={key}
//                   title={item.title}
//                   img={item.url}
//                   article={item.article}
//                   openModal={() =>this.changeModal(item.article)}
//                   color={item.color}
//                   price={item.price}
//                 />
//               )
//             })}
//           />
//         </section>
//       </>
//     )
//   }
// }

export default App
