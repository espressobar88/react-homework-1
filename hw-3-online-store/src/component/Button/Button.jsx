import React from "react";
import "../../assets/styles/Button.scss";
import PropTypes from "prop-types"


 const Button = (props) => {
  const { text, color, openModal } = props;

  return (
    <button
        className="button"
        onClick={openModal}
        style={{ backgroundColor: color }} >
        {text}
      </button>
  )
}

Button.propTypes = {
  text: PropTypes.string,
  color : PropTypes.string,
  openModal : () => {},
  closeModal : () => {},
  actions : () => {}
}

Button.defaultProps = {
text :  'add to card'
}
export default Button;
