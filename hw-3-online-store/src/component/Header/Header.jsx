import React from 'react'
import { Switch,Route,NavLink, BrowserRouter } from 'react-router-dom'
import FavoritPage from '../Main/pages/Favorit/FavoritPage'
import ShopBasketPage from '../Main/pages/Basket/ShopBasketPage'
import ProductsList from '../Main/ProductsList/ProductsList'
 const Header = () => {
    return (      
    <header>
      <nav>
        <ul>
          <li><NavLink to="/home">Home</NavLink></li>
         <li><NavLink to="/favorit">Favorit</NavLink></li> 
          <li><NavLink to="/shopBasket">shopBasket</NavLink></li>
        </ul>

      </nav>

    </header>
    )
}

export default Header