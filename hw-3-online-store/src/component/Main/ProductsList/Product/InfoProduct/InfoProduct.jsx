import React,{useState,useEffect} from 'react'
import Button from '../../../../Button/Button'
 const InfoProduct = (props) => {
useEffect(() => {
  const getStatusFavorit = JSON.parse(localStorage.getItem("cardsFavorit"))
 getStatusFavorit.find((article) => {
   if( article == props.article) {
    setStatusFavorit(!isFavorit);
 }})
 
}, [])
  const [isFavorit , setStatusFavorit] = useState(false)
  const changeFavorit = (article) => {
    setStatusFavorit(!isFavorit);
    const getCard = JSON.parse(localStorage.getItem("cardsFavorit"));
    if(!isFavorit) { 
      getCard.push(article)
      localStorage.setItem("cardsFavorit" ,JSON.stringify(getCard))
      
    } else {
   const setCard = getCard.filter((product) => product !== article )
   localStorage.setItem("cardsFavorit" ,JSON.stringify(setCard))
    }
    
  }
  
    return (
        <div className="list_info">
          {props.action}
         
          <span>Артикул : {props.article}</span>

          <a onClick={() => changeFavorit(props.article)} className="info_icon_star">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
               fill={isFavorit ? "yellow" : "black"}
            >
              <path d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z" />
            </svg>
          </a>
          <span className="info_price">Цена : {props.price}</span>
        </div>
    )
}
export default InfoProduct 