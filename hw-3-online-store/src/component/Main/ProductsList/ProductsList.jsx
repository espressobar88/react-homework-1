import React from "react";
import "../../../assets/styles/ProductsList.scss";


 const ProductsList = (props) => {
  const { content } = props;
  return <ul className="product_list">{content}</ul>;
}




export default ProductsList;
