import React,{useEffect, useState} from 'react'
import ProductsList from '../../ProductsList/ProductsList'
import Product from '../../ProductsList/Product/Product'

 const FavoritPage = () => {


const AllProducts = JSON.parse(localStorage.getItem("cardsData"));
const favoritProductArticles = JSON.parse(localStorage.getItem("cardsFavorit"));
let favoritProduct = AllProducts.filter((product) => favoritProductArticles.includes(product.article))

    return (
        <div>
    <ProductsList
            content={favoritProduct.map((item, key) => {  
              
              return (
                      <Product
                  key={key}
                  title={item.title}
                  img={item.url}
                  article={item.article}
                  openModal={() => setChangeOpenModal(!changeOpenModal)}
                  color={item.color}
                  price={item.price}
                 />
              )
            })}
          />
        </div>
    )
}
export default FavoritPage