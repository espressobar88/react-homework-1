import React,{useState} from 'react'
import ProductsList from '../../ProductsList/ProductsList'
import Product from '../../ProductsList/Product/Product'
import Modal from '../../../Modal/Modal'
import Button from '../../../Button/Button'

 const ShopBasketPage = () => {
     
    const AllProducts = JSON.parse(localStorage.getItem("cardsData"));
    const cardsInShopProductArticles = JSON.parse(localStorage.getItem("cardsInShop"));
    let cardsInShopProduct = AllProducts.filter((product) => cardsInShopProductArticles.includes(product.article))
    let [changeOpenModal , setChangeOpenModal] = useState(false)

const changeModal = (article) => {
    setChangeOpenModal(!changeOpenModal)
   
}

const removeCard = (article) => {
  setChangeOpenModal(!changeOpenModal)

}
        return (
            <div>           
    {changeOpenModal && ( <Modal header={'Подумай дважды'} text={'Удалить продукт из корзины'}
    closeButton={true}  closeModal={() => setChangeOpenModal(!changeOpenModal)} actions={
      <>
        <Button openModal={() => removeCard()} text={'ok'}
          color={'grey'}/>
        <Button openModal={() => setChangeOpenModal(!changeOpenModal)} text={'cancel'} color={'grey'} />
      </>
    } />
)}
        <ProductsList
                content={cardsInShopProduct.map((item, key) => {  
                  return (
                          <Product
                      key={key}
                      title={item.title}
                      img={item.url}
                      article={item.article}
                      color={item.color}
                      price={item.price}
                      closeButton={true}
                      
                      removeShopCard={() => changeModal(item.article)}
                     />
                  )
                })}
              />
            </div>
        )
}
export default ShopBasketPage